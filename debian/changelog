ibus-input-pad (1.4.99.20140916-3) unstable; urgency=medium

  * Team upload
  * d/clean: Allow one to build twice (closes: #1048161)
  * Lintian override about no-manual-page
  * Bump Standards-Version to 4.6.2

 -- Gunnar Hjalmarsson <gunnarhj@debian.org>  Sun, 27 Aug 2023 16:50:32 +0200

ibus-input-pad (1.4.99.20140916-2) unstable; urgency=medium

  * Team upload.
  * debian/patches: Add patch from Chris Lamb to make the build
    reproducible. (Closes: #998312)

 -- Boyuan Yang <byang@debian.org>  Tue, 07 Dec 2021 15:21:24 -0500

ibus-input-pad (1.4.99.20140916-1) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Boyuan Yang <byang@debian.org>  Sat, 30 Oct 2021 20:04:54 -0400

ibus-input-pad (1.4.99.20140916-1~exp1) experimental; urgency=medium

  * Team upload.
  * New upstream version 1.4.99.20140916.
  * Bump debhelper compat to v13.
  * Migrate to /usr/libexec. (Closes: #955229)
  * Require libinput-pad (>= 1.0.99).

 -- Boyuan Yang <byang@debian.org>  Sat, 30 Oct 2021 19:46:24 -0400

ibus-input-pad (1.4.2-2) unstable; urgency=medium

  * Team upload.
  * debian: Apply "wrap-and-sort -abst".
  * debian/control:
    + Update maintainer field and use Debian Input Method Team.
      (Closes: #899933)
    + Update uploaders list and remove LI Daobing.
      (Closes: #841803)
    + Bump debhelper compat to v11.
    + Bump Standards-Version to 4.2.1.
    + Fix homepage field and Vcs-* fields.
  * debian/rules: Use "dh_missing --fail-missing" and full hardening.
  * debian/copyright: Update upstream url.
  * debian/watch: Monitor upstream GitHub project.

 -- Boyuan Yang <byang@debian.org>  Sat, 24 Nov 2018 11:22:33 -0500

ibus-input-pad (1.4.2-1) unstable; urgency=medium

  * New upstream release
  * Update watch file and add me as uploader.
  * Use dh-autoreconf. Closes: #727387
  * Use hardening.
  * Bump Standard-Version to 3.9.5.

 -- Osamu Aoki <osamu@debian.org>  Sun, 26 Jan 2014 22:52:45 +0900

ibus-input-pad (1.4.0-4) unstable; urgency=low

  * Team upload.
  * Build with GTK+3 using libgtk-3-dev.  Closes: #724226

 -- Osamu Aoki <osamu@debian.org>  Sun, 06 Oct 2013 10:55:25 +0900

ibus-input-pad (1.4.0-3) unstable; urgency=low

  * Team upload.
  * Fix libexecdir to match ibus-setup expectation.
    Closes: #712577

 -- Osamu Aoki <osamu@debian.org>  Wed, 19 Jun 2013 21:07:22 +0900

ibus-input-pad (1.4.0-2) unstable; urgency=low

  * Build-Depends on libibus-1.0-dev instead of libibus-dev

 -- Asias He <asias.hejun@gmail.com>  Tue, 26 Jul 2011 10:11:13 +0800

ibus-input-pad (1.4.0-1) unstable; urgency=low

  * New upstream release
  * Drop debian/ibus-input-pad.install since it is a single binary
    pacakge.
  * Bump Standard-Version to 3.9.2
  * Simplify debian/rules

 -- Asias He <asias.hejun@gmail.com>  Sun, 24 Jul 2011 19:06:35 +0800

ibus-input-pad (0.1.4-1) unstable; urgency=low

  * Set Vcs to git.debian.org
  * Drop dependence on libinput-pad
  * Add libinput-pad-dev version (>= 1.0.0)

 -- Asias He <asias.hejun@gmail.com>  Thu, 24 Mar 2011 22:52:59 +0800

ibus-input-pad (0.1.3-1) unstable; urgency=low

  * Initial release (Closes: #593950)

 -- Asias He <asias.hejun@gmail.com>  Mon, 23 Aug 2010 13:22:35 +0800
